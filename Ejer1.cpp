
#include <stdio.h> 
#include <stdlib.h>
#include <omp.h>	// OpenMP

int main() {
	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif

	int a[100], b[100];

	for( int i=0; i<100; i++ ) a[i] = rand();
	for( int i=0; i<100; i++ ) b[i] = rand();

	// TO DO

	return 0;
} 
