
#include <stdio.h> 
#include <omp.h>	// OpenMP

int main() {

	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif
	int a[5],b[5];
	for( int i=0; i<5; i++ ){
		a[i] = i;
		b[i] = i;
	}

	// TO DO
	for( int i=1; i<5; i++ ){
		a[i] = a[i-1] - 1;
		b[i] = a[i] * 2;
	}

	return 0;
} 
