
#include <math.h>
#include <stdio.h> 
#include <omp.h>	// OpenMP

void mostrar_vector( float *v, int n ){
	for(int i=0; i<n; i++ )
		printf("%2.2f ", v[i] );
	printf("\n");
}

int main() {

#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif

	float a[50],b[50], y[20],z[20];
	int i;

	for( i=0; i<50; i++ ){
		a[i] = (float) i;
		b[i] = (float) i;
	}
	for( i=0; i<20; i++ ){
		y[i] = (float) i;
		z[i] = (float) i;
	}

	// TO DO
	#pragma omp parallel for
	for (i=1; i<50; i++)
		b[i] = (a[i] + a[i-1]) / 2.0;
	#pragma omp parallel for
	for (i=0; i<20; i++)
		y[i] = sqrt(z[i]);

	mostrar_vector( b, 50 );
	mostrar_vector( y, 20 );

	return 0;
} 
