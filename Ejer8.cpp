
#include <stdio.h> 
#include <stdlib.h>
#include <omp.h>	// OpenMP

int main() {
	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif


	int a[100], b[100];

	for( int i=0; i<100; i++ ) a[i] = rand();
	for( int i=0; i<100; i++ ) b[i] = rand();

	// Supuesto 1
	for (int i=0; i<100; i++)
		b[i] = a[i] / 2;

	// Supuesto 2
	for (int i=0; i<100; i++)
		b[i] = a[i] / 2;

	// Supuesto 3
	for (int i=0; i<100; i++)
		b[i] = a[i] / 2;

	// Supuesto 4
	for (int i=0; i<100; i++)
		b[i] = a[i] / 2;


	return 0;
} 
