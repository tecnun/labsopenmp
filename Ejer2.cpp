
#include <stdio.h> 
#include <stdlib.h>
#include <omp.h>	// OpenMP

void mostrar_vector( int *v, int n ){
	for(int i=0; i<n; i++ )
		printf("%d ", v[i] );
	printf("\n");
}

int main() {
	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif


	int a[100], b[100];
	
	// TO DO
	int i;
	for( i=0; i<100; i++ )
		a[i] = i;
	for( i=0; i<100; i++ )
		b[i] = 100-i;
	for ( i=0; i<100; i++)
		b[i] = a[i] * b[i];

	mostrar_vector( a, 100 );
	mostrar_vector( b, 100 );

	return 0;
} 
