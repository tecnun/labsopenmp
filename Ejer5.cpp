
#include <stdio.h> 
#include <omp.h>	// OpenMP

int main() {

	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif

	int a[100];
	for( int i=0; i<100; i++ )
		a[i] = i;
		
	int contador = 0;

	// TO DO
	for( int i=0; i<100; i++ ){
		if( a[i]%2 == 0 ){
			contador++;
		}
	}

	return 0;
} 
