
#include <stdio.h> 

void main() {
	
#if defined(_OPENMP)
	printf("OpenMP? Yes\n\n");
#else
	printf("OpenMP? No\n\n");
#endif


	int a[100];
	int sum, delta;

	for( int i=0; i<100; i++ ) a[i] = 1;
	sum = 0;
	delta = 4;

	// TO DO
	for( int i=0; i<100; i++ )
		sum = sum + a[i] + delta;
}
